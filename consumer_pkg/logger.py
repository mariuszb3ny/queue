class Logger:
    """ Simple logger class - logs requests failures to failed_requests.txt

    """

    @staticmethod
    def log_error(url, error):
        """
        :param url: request url
        :param error
        """
        with open("failed_requests.txt", "a") as errors:
            errors.write("Request to {0} failed - error: {1}\n".format(url, error))
