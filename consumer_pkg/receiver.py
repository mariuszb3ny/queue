class Receiver:
    """ Class responsible for listening for the incoming messages

    Args:
        queue (queue instance)
        handler (handler instance)
        logger (logger instance)
    """
    def __init__(self, queue, handler, logger):
        self.queue_instance = queue
        self.content_handler = handler
        self.logger = logger

    def wait_for_messages(self):
        """
        Recursive method - call repeated every 20 seconds
        (long polling queue settings)
        """
        for message in self.queue_instance.receive_messages():
            self.content_handler.make_call(message.body, self.logger)
            message.delete(message.receipt_handle)

        self.wait_for_messages()
