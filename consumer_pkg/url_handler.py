import urllib.request


class UrlHandler:
    """ Handles requests to urls fetched from the queue

    """

    @staticmethod
    def make_call(url, logger):
        """

        :param url: request url
        :param logger: logger instance
        """
        try:
            request_data = urllib.request.urlopen(url)
            print("{0} {1} {2}".format(url, request_data.status, len(request_data.read())))

        except (urllib.error.URLError, ValueError) as e:
            print("Request to {} failed. Logged in failed_requests.txt".format(url))
            logger.log_error(url, e)
