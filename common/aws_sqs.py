import boto3
import time
from botocore.exceptions import ClientError, EndpointConnectionError
from boto3.exceptions import ResourceNotExistsError
from common.exceptions import AwsException


class AwsSqs:
    """ Class responsible for setting up connection to amazon SQS service

    Args:
        region_name (str): Region where amazon resource is set up.
        service (str): type of the service to connect to
        access_key (str)
        secret_key (srt)
    """

    def __init__(self, region_name, service, access_key, secret_key):
        self.region_name = region_name
        self.service = service
        self.access_key = access_key
        self.secret_key = secret_key

    def send_message(self, message):
        """
        As SQS has 5 minutes deduplication policy - MessageDeduplicationId has to be send with
        the message to allow sending same messages in the 5 minutes window


        :param message: message to be sent to SQS
        :raises: AwsException
        :return: queue response including messageId and MD5 of message content
        """
        try:
            queue = self.get_queue_instance()
        except ClientError as e:
            raise AwsException(e.response['Error']['Message'], e.response['ResponseMetadata']['HTTPStatusCode'])

        except EndpointConnectionError:
            raise AwsException('Endpoint does not exist', 500)

        except ResourceNotExistsError:
            raise AwsException('Resource does not exist', 500)

        return queue.send_message(
            MessageBody=message,
            MessageGroupId='urls',
            MessageDeduplicationId=str(int(round(time.time() * 1000)))
        )

    def get_queue_instance(self):
        sqs = boto3.setup_default_session(region_name=self.region_name)

        sqs = boto3.resource(self.service,
                             aws_access_key_id=self.access_key,
                             aws_secret_access_key=self.secret_key
                             )

        return sqs.get_queue_by_name(QueueName='test_queue.fifo')
