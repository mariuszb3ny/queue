"""
Custom exceptions
"""


class AwsException(Exception):
    def __init__(self, msg, http_code):
        self.message = msg
        self.HTTP_code = http_code


class PayloadException(Exception):
    def __init__(self, msg, http_code):
        self.message = msg
        self.HTTP_code = http_code
