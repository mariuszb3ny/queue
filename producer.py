from flask import Flask
from producer_pkg.check_handler import check_handler

app = Flask(__name__)

app.register_blueprint(check_handler)


@app.errorhandler(404)
def not_found(e):
    return 'Route not found', 404


if __name__ == '__main__':
    app.run(debug=True)
