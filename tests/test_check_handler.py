import os
import sys

topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)

from producer import app
import pytest
import json


@pytest.fixture
def client():
    return app.test_client()


def test_route_not_found(client):
    response = client.get('/other')
    assert response.data == b'Route not found'

    response = client.get('/')
    assert response.data == b'Route not found'


def test_incorrect_payload(client):
    response = client.post('/check', json={'attr': 'data'})
    response_dict = json.loads(response.data)
    assert response_dict['responseCode'] == 400


def test_incorrect_url(client):
    response = client.post('/check', json={'url': 'www.test.com'})
    response_dict = json.loads(response.data)
    assert response_dict['responseCode'] == 422

    response = client.post('/check', json={'url': 'http:www.test.com'})
    response_dict = json.loads(response.data)
    assert response_dict['responseCode'] == 422

    response = client.post('/check', json={'url': 'www.test'})
    response_dict = json.loads(response.data)
    assert response_dict['responseCode'] == 422
