#### Project setup
- clone the repo
- run ```pip install -r requirements.txt``` in the root directory
- to run the producer ```python producer.py```
- to run the consumer ```python consumer.py```
- to run tests ```pytest```

To add the message to the queue:

```
curl -XPOST -H "Content-type: application/json" -d '{

"url": "https://www.bbc.com/news",
}' 'http://127.0.0.1:5000/check'
```

All other routes, malformed payload, or incorrectly formatted urls will - should :) be rejected.

Once url is added to the queue, consumer should fetch the message and display the url, http code and length of the content

All failures are logged in failed_requests.txt

#### Notes
Amazon SQS is used as a fifo queue service. The producer is build on the flask micro framework as it allows simple route control. The consumer is just simple python script. Project was created using python 3.6