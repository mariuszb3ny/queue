import re
from common.exceptions import PayloadException


class PayloadCheck:
    """API payload check helper

    """

    @staticmethod
    def check_payload(data):
        """ checks if api payload is correct

        :param data: request body
        :raises: PayloadException - if payload or url is malformed
        """
        if 'url' not in data:
            raise PayloadException('Missing parameter - url', 400)

        if not re.match(r"^https?:\/\/www\.([\.\/\w-]*)$", data['url']):
            raise PayloadException('Provided url is incorrect, expected: http://example.com', 422)
