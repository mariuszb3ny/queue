from flask import Blueprint, request, jsonify
from config import AWS_CONFIG
from .payload_validator import PayloadCheck
from common.aws_sqs import AwsSqs
from common.exceptions import PayloadException, AwsException

"""Check handler blueprint

Checks if  APi payload is correct and attempts to send message to the  queue
If payload is incorrect or something goes wrong - all exception are bing handled in this blueprint
"""
check_handler = Blueprint('check_handler', __name__)


@check_handler.route('/check', methods=['POST'])
def check():
    response_code = 200
    payload = request.get_json()
    queue = AwsSqs(**AWS_CONFIG)

    try:
        PayloadCheck.check_payload(payload)
        queue.send_message(payload['url'])

    except (AwsException, PayloadException) as e:
        response = {
            'status': 'error',
            'responseCode': e.HTTP_code,
            'message': e.message
        }
        response_code = e.HTTP_code

    else:
        response = {
            'status': 'ok'
        }

    return jsonify(response), response_code

