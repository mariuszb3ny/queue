from common.aws_sqs import AwsSqs
from config import AWS_CONFIG
from consumer_pkg.receiver import Receiver
from consumer_pkg.url_handler import UrlHandler
from consumer_pkg.logger import Logger

aws = AwsSqs(**AWS_CONFIG)
queue = aws.get_queue_instance()
receiver = Receiver(queue, UrlHandler, Logger)

print('Listening for messages...')

receiver.wait_for_messages()
